package net.emecas.poc.assertj.target;

import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.assertj.swing.junit.testcase.AssertJSwingJUnitTestCase;
import org.junit.Test;

public class TargetAppAssertJSwingTest extends AssertJSwingJUnitTestCase {

	private FrameFixture window;

	@Override
	public void onSetUp() {
		TargetApp target = GuiActionRunner.execute(() -> new TargetApp());
		window = new FrameFixture(robot(), target);
		window.show();
	}

	@Test
	public void shouldCopyTextInLabelWhenClickingButton() {
		window.textBox("textToCopy").enterText("Some random text");
		window.button("copyButton").click();
		window.label("copiedText").requireText("Some random text");
	}

}
