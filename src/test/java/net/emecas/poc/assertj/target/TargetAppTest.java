package net.emecas.poc.assertj.target;

import org.assertj.swing.edt.GuiActionRunner;
import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TargetAppTest {
	
	private FrameFixture window;

	@Before
	public void setUp() {
	  TargetApp target = GuiActionRunner.execute(() -> new TargetApp());
	  window = new FrameFixture(target);
	  window.show(); 
	}
	
	@After
	public void tearDown() {
	  window.cleanUp();
	}
	
	@Test
	public void shouldCopyTextInLabelWhenClickingButton() {
	  window.textBox("textToCopy").enterText("Some random text");
	  window.button("copyButton").click();
	  window.label("copiedText").requireText("Some random text");
	}

}
