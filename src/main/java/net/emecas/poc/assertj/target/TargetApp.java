package net.emecas.poc.assertj.target;

import static javax.swing.SwingUtilities.invokeAndWait;


import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import net.miginfocom.layout.AC;
import net.miginfocom.layout.LC;
import net.miginfocom.swing.MigLayout;



public class TargetApp extends JFrame {
  private static final long serialVersionUID = 1L;

  public TargetApp() {
    setMiglayout(new LC().wrapAfter(1), new AC().align("center"), new AC());

    final JTextField textField = TextFieldUtil.newTextField("textToCopy");
    JButton button = ButtonUtil.newButton("copyButton", "Copy text to label");
    final JLabel label = LabelUtil.newLabel("copiedText");

    ButtonUtil.addActionToButton(button, new Runnable() {

      @Override
      public void run() {
        label.setText(textField.getText());
      }
    });

    add(textField);
    add(button);
    add(label);

    pack();
  }

  public static void main(String[] args) throws InvocationTargetException, InterruptedException {
    invokeAndWait(new Runnable() {
      @Override
      public void run() {
        JFrame frame = new TargetApp();
        frame.setVisible(true);
      }
    });
  }
  
  public void setMiglayout(LC layout, AC columns, AC rows) {
	    setLayout(new MigLayout(layout, columns, rows));
	}
}